#include "adc.h"

void HAL_ADC_MspInit(ADC_HandleTypeDef *adcHandle);

void ADC1_Init(void)
{
  ADC_ChannelConfTypeDef sConfig = {0};

  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_Init(&hadc1) != HAL_OK) 
  {
    Error_handler();
  }

  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) 
  {
    Error_handler();
  }

  HAL_ADC_MspInit(&hadc1);
}

void HAL_ADC_MspInit(ADC_HandleTypeDef *adcHandle)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(adcHandle->Instance==ADC1)
  {
    __HAL_RCC_ADC1_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();

    GPIO_InitStruct.Pin = ADC_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    
    HAL_GPIO_Init(ADC_Port, &GPIO_InitStruct);
  }
}

uint16_t map(uint16_t val, uint16_t inMin, uint16_t inMax, uint16_t outMin, uint16_t outMax)
{
	return ((((val - inMin)*(outMax - outMin))/(inMax - inMin)) + outMin);
}
