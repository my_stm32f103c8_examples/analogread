/******************************************************************************
* File:             gpio.c
*
* Author:           Benjamin James  
* Created:          05/23/21 
* Description:      
*****************************************************************************/

#include "stm32f1xx_hal.h"

void GPIO_Init(void) 
{
  __HAL_RCC_TIM4_CLK_ENABLE();
  __HAL_RCC_ADC1_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
}
