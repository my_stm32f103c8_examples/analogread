/******************************************************************************
* File:             main.c
*
* Author:           Benjamin James  
* Created:          2021-05-23 
* Description:      
*****************************************************************************/

#include "main.h"
#include "gpio.h"
#include "sys.h"
#include "adc.h"
#include "tim.h"

uint32_t val = 0;

int main(void)
{
  HAL_Init();
  SysClk_Config();
  GPIO_Init();
  TIM4_Init();
  ADC1_Init();

  HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_4);

  HAL_ADCEx_Calibration_Start(&hadc1);

  while (1) {
    HAL_ADC_Start(&hadc1);
    HAL_ADC_PollForConversion(&hadc1, 1);
    val = HAL_ADC_GetValue(&hadc1);
    val = (val << 4);
    val = map(val, 0, 65532, 0, 1023);
    TIM4->CCR4 = val;
    HAL_Delay(1);
  }
}

// Function to configure system clock
void SysClk_Config(void) {
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;    // Selecting Oscillator type
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;                      // Enabling High Speed External Oscilator
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;       // 8MHz / 1 = 8MHz
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;                  // Enabling Phase Locked Loop 
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;          // Selecting Oscillator input to PLL
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;                  // 8MHz * 9 = 72MHz

  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
    Error_handler();
  }

  // Selecting clock type
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;     // Selecting SysClock source
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;            // Setting HCLK divider 72MHz / 1 = 72MHz 
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;             // Setting PCLK1 divider 72MHz / 2 = 36MHz
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;             // Setting PCLK2 divider 72MHz / 1 = 72MHz

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
    Error_handler();
  }

  HAL_RCC_EnableCSS();
}

// Error handling block
void Error_handler(void) {
  __disable_irq();
  while (1) {
    
  }
}
