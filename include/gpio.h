/******************************************************************************
* File:             gpio.h
*
* Author:           Benjamin James  
* Created:          05/23/21 
* Description:      
*****************************************************************************/

#ifndef __GPIO_H__
#define __GPIO_H__

void GPIO_Init(void);

#endif /* ifndef __GPIO_H__ */
