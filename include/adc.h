#ifndef __ADC_H__
#define __ADC_H__

#include "main.h"

void ADC1_Init(void);

uint16_t map(uint16_t val, uint16_t inMin, uint16_t inMax, uint16_t outMin, uint16_t outMax);

#endif /* ifndef __ADC_H__ */
