#ifndef __MAIN_H
#define __MAIN_H

#include <stm32f1xx_hal.h>

TIM_HandleTypeDef htim4;
ADC_HandleTypeDef hadc1;

void Error_handler(void);
void SysClk_Config(void);

#define PWM_Port GPIOB
#define PWM_Pin GPIO_PIN_9

#define ADC_Port GPIOA
#define ADC_Pin GPIO_PIN_0

#endif
